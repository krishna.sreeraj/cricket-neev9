public class Match {

    private final Batsman batsman;
    private final Bowler bowler;
    private final int targetToBeAchieved;
    private int totalScore;
    private final int overs;


    public Match(Batsman batsman, Bowler bowler, int targetScore, int overs) {
        this.batsman = batsman;
        this.bowler = bowler;
        this.targetToBeAchieved = targetScore;
        this.overs = overs;
        this.totalScore = 0;
    }

    public void start() {

        int BALLS_PER_OVER = 6;
        int totalNoOfBallsToBeBowled = BALLS_PER_OVER * overs;
        for (int ball = 0; ball < totalNoOfBallsToBeBowled; ball++) {
            if(!wonMatch()) {
                int bowlerScore = bowler.bowl();
                int batsmanScore = batsman.bat();
                if(batsmanScore == bowlerScore)
                    return;
                currentTotalScore(batsmanScore);
            }
            else return;
        }

    }
    private void currentTotalScore(int currentScore){
        totalScore+=currentScore;
    }

    public boolean wonMatch(){
        return totalScore >= targetToBeAchieved;
    }

}
