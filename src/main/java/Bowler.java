import java.util.Random;

public class Bowler {

    private static final int UPPERBOUND = 6;

    public int bowl(){
        Random random = new Random();
        return random.nextInt(UPPERBOUND);
    }

}
