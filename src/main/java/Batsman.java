import java.util.Random;

public class Batsman{

    private final BatsmanType batsmanType;
    private static final int UPPERBOUND = 6;

    Batsman(BatsmanType batsmanType){
        this.batsmanType =batsmanType;

    }


    public int bat() {
        Random random = new Random();
        if(batsmanType.equals(BatsmanType.HITTER))
        {
            int[] possibleScores = {0, 4, 6};
            return possibleScores[random.nextInt(possibleScores.length)];
        }
        return random.nextInt(UPPERBOUND);
    }

}
