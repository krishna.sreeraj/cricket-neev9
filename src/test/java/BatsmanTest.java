import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class BatsmanTest {

    @Test
    void whenBatsManIsAHitterBatsmanPermittedScoresShouldBeZeroOrFourOrSix(){
        Batsman batsman = new Batsman(BatsmanType.HITTER);
        List<Integer> possibleScores = new ArrayList<>();

        possibleScores.add(0);possibleScores.add(4);possibleScores.add(6);

        assertTrue(possibleScores.contains(batsman.bat()));

    }
    @Test
    void whenBatsManIsANormalBatsmanPermittedScoresShouldBeZeroOrFourOrSix(){
        Batsman batsman = new Batsman(BatsmanType.NORMAL);
        List<Integer> possibleScores = new ArrayList<>();

        possibleScores.add(0);possibleScores.add(1);possibleScores.add(2);
        possibleScores.add(3);possibleScores.add(4);possibleScores.add(5);possibleScores.add(6);

        assertTrue(possibleScores.contains(batsman.bat()));

    }
}
