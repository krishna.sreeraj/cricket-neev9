import org.junit.jupiter.api.Test;
import org.mockito.MockSettings;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

public class MatchTest {

    @Test
    void whenTotalScoreIsGreaterThanOrEqualsTargetScoreThenBatsmanWins() {
        int targetScore = 10;
        int overs = 2;
        Batsman mockBatsman = Mockito.mock(Batsman.class);
        Bowler mockBowler = Mockito.mock(Bowler.class);
        Match match = new Match(mockBatsman, mockBowler, targetScore, overs);

        when(mockBowler.bowl()).thenReturn(3);
        when(mockBatsman.bat()).thenReturn(2);
        match.start();

        assertTrue(match.wonMatch());

    }

    @Test
    void whenTotalScoreIsLessThanTargetScoreThenBatsmanLoses() {
        int targetScore = 10;
        int overs = 1;
        Batsman mockBatsman = Mockito.mock(Batsman.class);
        Bowler mockBowler = Mockito.mock(Bowler.class);
        Match match = new Match(mockBatsman, mockBowler, targetScore, overs);

        when(mockBowler.bowl()).thenReturn(2);
        when(mockBatsman.bat()).thenReturn(1);
        match.start();

        assertFalse(match.wonMatch());
    }

    @Test
    void whenBowlerAndBatsmanDeliversTheSameNumberThenBatsmanLoses() {

        int targetScore = 10;
        int overs = 1;
        Batsman mockBatsman = Mockito.mock(Batsman.class);
        Bowler mockBowler = Mockito.mock(Bowler.class);
        Match match = new Match(mockBatsman, mockBowler, targetScore, overs);

        when(mockBowler.bowl()).thenReturn(3);
        when(mockBatsman.bat()).thenReturn(3);
        match.start();

        assertFalse(match.wonMatch());
    }

    @Test
    void whenTotalScoreIsGreaterThanOrEqualsTargetScoreAndBatsmanIsHitterTypeThenBatsmanWins() {
        int targetScore = 24;
        int overs = 1;
        MockSettings mockSettings = withSettings().useConstructor(BatsmanType.HITTER);
        Batsman mockBatsman = Mockito.mock(Batsman.class,mockSettings);
        Bowler mockBowler = Mockito.mock(Bowler.class);
        Match match = new Match(mockBatsman, mockBowler, targetScore, overs);

        when(mockBowler.bowl()).thenReturn(3);
        when(mockBatsman.bat()).thenReturn(6);
        match.start();

        assertTrue(match.wonMatch());

    }

}
